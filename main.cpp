#include "mainwindow.h"
#include <QApplication>
#include <QLabel>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QLabel *lable = new QLabel("Hello World");
    lable -> show();
    lable -> resize(300, 150);
    lable -> setAlignment(Qt::AlignCenter);
    return a.exec();
}
